# CRIAR usuario Ansible com Password
ansible-playbook ansible-user.yaml -i inventory/kubernetes.yaml --user=upravink --ask-become-pass

# CRIAR usuario Ansible com KeyPair
ansible-playbook ansible-user.yaml -i inventory/zabbix-aws.yaml --user=centos --key-file "keys/AWS-MASTER.pem"

